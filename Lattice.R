## Lattice
library(sp)
library(maptools)
library(spdep)
data(package="spdep")

citation("spdep")

# vignette "Introduction to the North Carolina SIDS data set" 
# by Roger Bivand in 2011-04-12
nc_file <- system.file("etc/shapes/sids.shp", package = "spdep")[1]
llCRS <- CRS("+proj=longlat +datum=NAD27")
nc <- readShapeSpatial(nc_file, ID = "FIPSNO", proj4string = llCRS)

str(nc@data)

nc$BIR74k <- nc$BIR74/1000

pdf("TeX/figures/BIRk_nc.pdf", width=6, height=4)
spplot(nc, "BIR74k", scales=list(draw=TRUE),
       col.regions=bpy.colors(), main="Number of thousand births 1974-1978")
dev.off()

# probability map
ncpmap <- probmap(nc$SID74, nc$BIR74)
nc$pmap <- ncpmap$pmap

pdf("TeX/figures/nc_probabilityMap.pdf", width=6, height=3)
spplot(nc, "pmap", col.regions=bpy.colors(20), 
       at=c(0,1,2.5,5,10,25,50,75,90,95,97.5,99,100)/100, 
       scales=list(draw=TRUE))
dev.off()


# neighbourhoods as in Cressie
gal_file <- system.file("etc/weights/ncCC89.gal", package = "spdep")[1]
ncCC89 <- read.gal(gal_file, region.id = nc$FIPSNO)
ncCC89

pdf("TeX/figures/nc_neighbourhoods.pdf", width=6, height=3)
par(mai=c(1,1,0,0))
plot(nc, border = "grey", axes=TRUE, las=1, asp=1)
plot(ncCC89, coordinates(nc), add = TRUE, col = "blue")
dev.off()

# plot transformed data
nc$ft.SID74 <- sqrt(1000)*(sqrt(nc.sids$SID74/nc.sids$BIR74) +
                             sqrt((nc.sids$SID74+1)/nc.sids$BIR74))

pdf("TeX/figures/nc_FTtransformed.pdf", , width=6, height=3)
spplot(nc, "ft.SID74", cuts = 12, col.regions = bpy.colors(),
       scales=list(draw=TRUE), at=0:7)
dev.off()

# example spautolm
example(nc.sids)
ft.SID74 <- sqrt(1000)*(sqrt(nc.sids$SID74/nc.sids$BIR74) +
                          sqrt((nc.sids$SID74+1)/nc.sids$BIR74))
lm_nc <- lm(ft.SID74 ~ 1)
sids.nhbr30 <- dnearneigh(cbind(nc.sids$east, nc.sids$north), 0, 30, row.names=row.names(nc.sids))
sids.nhbr30.dist <- nbdists(sids.nhbr30, cbind(nc.sids$east, nc.sids$north))
sids.nhbr <- listw2sn(nb2listw(sids.nhbr30, glist=sids.nhbr30.dist, style="B", zero.policy=TRUE))
dij <- sids.nhbr[,3]
n <- nc.sids$BIR74
el1 <- min(dij)/dij
el2 <- sqrt(n[sids.nhbr$to]/n[sids.nhbr$from])
sids.nhbr$weights <- el1*el2
sids.nhbr.listw <- sn2listw(sids.nhbr)
both <- factor(paste(nc.sids$L_id, nc.sids$M_id, sep=":"))
ft.NWBIR74 <- sqrt(1000)*(sqrt(nc.sids$NWBIR74/nc.sids$BIR74) +
                            sqrt((nc.sids$NWBIR74+1)/nc.sids$BIR74))
mdata <- data.frame(both, ft.NWBIR74, ft.SID74, BIR74=nc.sids$BIR74)
outl <- which.max(rstandard(lm_nc))
as.character(nc.sids$names[outl])
mdata.4 <- mdata[-outl,]
W <- listw2mat(sids.nhbr.listw)
W.4 <- W[-outl, -outl]
sids.nhbr.listw.4 <- mat2listw(W.4)

##
print(sids.nhbr.listw.4, zero.policy=TRUE)
car <- spautolm(ft.SID74 ~ 1, data=mdata.4, listw=sids.nhbr.listw.4,
                weights=BIR74, family="CAR")
summary(car)

str(mdata.4)
str(sids.nhbr.listw.4)

# plot fitted values
nc$fitCAR <- NA
nc$fitCAR[-outl] <- fitted.values(car)

pdf("TeX/figures/nc_CAR.pdf", , width=6, height=3)
spplot(nc, "fitCAR", cuts = 12, col.regions = bpy.colors(),
       scales=list(draw=TRUE))#, at=0:7/2)
dev.off()

# SAR
sids.nhbr30.invDist <- lapply(sids.nhbr30.dist, function(x) 1/x)
sids.sar.nhbr.listw <- nb2listw(sids.nhbr30 ,glist=sids.nhbr30.invDist, style="W",zero.policy=TRUE)
W.sar <- listw2mat(sids.sar.nhbr.listw)
W.sar.4 <- W.sar[-outl, -outl]
sids.sar.nhbr.listw4 <- mat2listw(W.sar.4)

sar <- spautolm(ft.SID74 ~ 1, data=mdata.4, 
                listw=sids.sar.nhbr.listw4, family="SAR",
                weights=BIR74)
summary(sar)

nc$fitSAR <- NA
nc$fitSAR[-outl] <- fitted.values(sar)

pdf("TeX/figures/nc_SAR.pdf", , width=6, height=3)
spplot(nc, "fitSAR", cuts = 12, col.regions = bpy.colors(),
       scales=list(draw=TRUE))#, at=0:7/2)
dev.off()