## Fields
library(sp)
library(gstat)
library(lattice)
data(package="gstat")

data(meuse)
coordinates(meuse) <- ~x+y

data(meuse.grid)
coordinates(meuse.grid) <- ~x+y
gridded(meuse.grid) <- TRUE

meuseObs <- spplot(meuse, "zinc", col.regions=bpy.colors(), cex=0.5, key.space="right",
                   cuts=0:18*100, auto.key=FALSE,
                   main="obs. zinc concentrations")

meuse.grid$zincIDW <- idw(zinc~1,meuse, meuse.grid)$var1.pred
meuseIDW <- spplot(meuse.grid, "zincIDW", col.regions=bpy.colors(),
                   at=0:18*100, main="interpol. zinc concentrations")


pdf("TeX/figures/meuseObsIDW.pdf",width=6, height=3.75)
print(meuseObs, pos=c(0,0,0.425,1), more=TRUE)
print(meuseIDW, pos=c(0.425,0,1,1), more=FALSE)
dev.off()


vgmMeuse <- variogram(zinc~1, meuse)
plot(vgmMeuse)

pdf("TeX/figures/meuseVgm.pdf",width=6, height=3.5)
plot(vgmMeuse)
dev.off()

head(vgm())

vgmSphMeuse <- fit.variogram(vgmMeuse, vgm(150000, "Sph", 1000, 40000))
vgmExpMeuse <- fit.variogram(vgmMeuse, vgm(150000, "Exp", 1000, 40000))
vgmLinMeuse <- fit.variogram(vgmMeuse, vgm(150000, "Lin", 1000, 40000))

pSph <- plot(vgmMeuse, vgmSphMeuse)
pExp <- plot(vgmMeuse, vgmExpMeuse, col="green")
pLin <- plot(vgmMeuse, vgmLinMeuse, col="red")

pdf("TeX/figures/meuseVgmModel.pdf",width=6, height=3.5)
print(pExp, more=TRUE)
print(pLin, more=TRUE)
print(pSph, more=FALSE)
dev.off()

gstat:::plot.gstatVariogram

vgmMeuse$Sph <- variogramLine(vgmSphMeuse, dist_vector=vgmMeuse$dist)



multVgm <- vgmMeuse[,c("gamma","dist")]
multVgm$ID <- "sample"

multVgm <- rbind(multVgm, data.frame(gamma=variogramLine(vgmSphMeuse, dist_vector=multVgm$dist)$gamma,
                                     dist=multVgm$dist, ID=rep("Sph",length(multVgm$dist))))

xyplot(gamma~dist|ID, multVgm, type=c("p","l"))

# vgmModelMeuse <- fit.variogram(vgmMeuse, vgm(0.6, "Sph", 1000, 0.1))

pdf("TeX/figures/meuseVgmModel.pdf",width=6, height=3.5)
plot(vgmMeuse, vgmSphMeuse)
dev.off()

xyplot (gamma~dist, multVgm[multVgm$ID=="Sph"], add=T)

meuseKriged <- krige(zinc~1,meuse, meuse.grid, model=vgmModelMeuse)
# meuseKriged$var1.exp <- exp(meuseKriged$var1.pred)
meuseKrige <- spplot(meuseKriged, "var1.pred", col.regions=bpy.colors,
                     at=0:18*100, main="kriged zinc concentrations")

pdf("TeX/figures/meuseObsKrige.pdf",width=6, height=3.75)
print(meuseObs, pos=c(0,0,0.425,1), more=TRUE)
print(meuseKrige, pos=c(0.425,0,1,1), more=FALSE)
dev.off()

# kriging variance

meuseVar <- spplot(meuseKriged, "var1.var", col.regions=bpy.colors,
                   main="kriging variance")

pdf("TeX/figures/meuseKrigeVar.pdf",width=6, height=3.75)
print(meuseKrige, pos=c(0,0,0.485,1), more=TRUE)
print(meuseVar, pos=c(0.485,0,1,1), more=FALSE)
dev.off()
