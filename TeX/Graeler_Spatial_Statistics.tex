\documentclass[compress,mathserif,table,href]{beamer}

\usepackage{animate}
\usepackage[numbers]{natbib}
\newcommand{\newblock}{}

\def\lecturename{IGSSE Forum 2014}
\input{talk-style.tex}


\lecture[1]{Spatial Statistics - a one hour introduction}

\date{June 17, 2014}

\graphicspath{{figures/}}

\begin{document}

\begin{frame}
\maketitle
\end{frame}		

\section{Spatial Data} % ~10 slides

\begin{frame}{Spatial Data}
From a purely statistical perspective, spatial data is multivariate data with special covariates: the coordinates. \vspace{4pt}

Tobler's first law of Geography states \citep{Tobler1970}: 

\begin{quote}
Everything is related to everything else, but near things are more related than distant things.
\end{quote}
\end{frame}

\begin{frame}{Coordinate Reference System}

We model the earth, but think in maps: locations are projected from a curved surface in 3D to flat 2D space.\vspace{4pt}

Be aware of geographic coordinates and different projections that maintain angles, certain distances or area.\vspace{4pt}

Imagine the following distances between:\\
\begin{itemize}
\item the Fjord of Oslo (59.85 N 10.75 E) and Uppsala (59.85~N 17.63~E) that are at the same latitude:
\begin{description}
\item[Degrees:] 6.88
\item[Great Circle:] 385~km
\item[Rate:] 56 km/degree
\end{description}
\item the intersections of the Congo river with the equator (0.00~N 18.21~E) and (0.00~N, 25.53~E):
\begin{description}
\item[Degrees:] 7.32	
\item[Great Circle:] 814~km
\item[Rate:] 111~km/degree
\end{description}
\end{itemize}
\end{frame}

\begin{frame}{CRS identifier}
To distinguish different projections, a well prepared data set comes with its coordinate reference system (CRS) as metadata. \vspace{4pt}

These are often encoded as
\begin{itemize}
\item<1-> EPSG-codes  (by the  European Petroleum Survey Group)
\item<1-> proj4string
\end{itemize}

They define how the reference surface (sphere, ellipsoid) is fixed to the real world (called the datum) and how the projection (surface in 3D to 2D plane) is made.
\end{frame}

\begin{frame}{Projection}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{projection}
\end{figure}
\end{frame}

\subsection{Point Patterns}
\begin{frame}{Point Patterns}
Records of locations of events are referred to as \emph{Point~Patterns} (e.g. locations of earthquakes). \vspace{4pt}

Adding an attribute (e.g. magnitude) to the raw locations generates a \emph{marked point pattern}.

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{eartquakesFiji}
\end{figure}
\end{frame}

\subsection{Lattice}
\begin{frame}{Lattice}
Data collected per region is referred to as \emph{lattice} data (e.g. number of birth per county).\vspace{4pt}

These values are true per region, but generally not observable at each point in that region.

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{BIRk_nc}
\end{figure}
\end{frame}

\subsection{Fields}

\begin{frame}{Fields}
\emph{Fields} are understood as continuously spreading over space (e.g. temperature recordings) and typically observed at a set of distinct locations and illustrated as interpolated maps. Typically, fields are modelled as a realisation of a spatial random field.

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{meuseObsIDW}
\end{figure}
\end{frame}

\begin{frame}{Transitions between data types}
\begin{itemize}
\item Points in a point pattern can be counted per pre-defined regions resulting in a lattice
(earthquakes per region).
\item Smoothing techniques can generate a point density out of a point pattern resulting in a field (density of earthquakes).
\item Fields can be aggregated to regions resulting in a lattice (average temperature per federal state).
\end{itemize}
\end{frame}
% Add more examples?

\section{Modelling} % ~40 slides

\begin{frame}{Motivation}

\begin{itemize}
\item a better understanding of the observed phenomenon 
\item prediction at unobserved locations
\item prediction of the future
\item studying driving factors 
\end{itemize}

\end{frame}
\subsection{Point Patterns}
\begin{frame}{Longleaf pine trees}
The following examples follow those from the spatstat package \cite{Baddeley2005}. More detailed explanations are found in \cite{Baddeley2010}.\vspace{-16pt}
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{longleaf}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Uniform intensity}
How many trees are there per square unit on average?
$$ \lambda=\frac{N}{area} $$
{\footnotesize
\begin{verbatim}
> summary(longleaf)
Marked planar point pattern:  584 points
Average intensity 0.0146 points per square metre

Coordinates are given to 1 decimal place
i.e. rounded to the nearest multiple of 0.1 metres

marks are  numeric,  of type ‘double’
Summary:
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
   2.00    9.10   26.15   26.84   42.12   75.90 

Window: rectangle = [0, 200] x [0, 200] metres
Window area = 40000 square metres
Unit of length: 1 metre 
\end{verbatim}}
\end{frame}

\begin{frame}{Inhomogeneous intensity}
How does the intensity change throughout the study area?
\begin{description}
\item[quadrat counting] the region is split into areas of equal size and a uniform density is estimated per area
\item[kernel smoothing] the contribution of each point is spread across its neighbourhood based on some kernel density being properly rescaled
\end{description}
\end{frame}

\begin{frame}[fragile]{quadrat counting}
\vspace{-8pt}
\footnotesize
\begin{verbatim}
> quadratcount(longleaf,nx=4,ny=4)
           x
y           [0,40] (40,80] (80,120] (120,160] (160,200]
  (160,200]     20      25       37         7        26
  (120,160]     25      34       50        51        27
  (80,120]      29      22       15        31        37
  (40,80]       26      12       24        19         8
  [0,40]        18      14       12         8         7
\end{verbatim}\vspace{-28pt}
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{longleafQcount}
\end{figure}

\end{frame}

\begin{frame}[fragile]{kernel smoothing}
\footnotesize
\begin{verbatim}
> density(longleaf)
\end{verbatim}
\vspace{-28pt}
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{longleafDensity}
\end{figure}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{Fitting Poisson processes}
Following the algorithm by \cite{Berman1992}, we seek a model where the intensity $\lambda$ is log-linear in the parameter $\theta$:
$$ \log\big(\lambda_{\theta}(x,y)\big) = \theta \cdot f(x,y)$$

A model fit with $f$ being a simple linear model of the coordinates, is obtained through
\footnotesize
\begin{verbatim}
> modelLl <- ppm(longleaf, ~x+y)
> modelLl
Nonstationary Poisson process

Trend formula: ~x + y
> AIC(modelLl)
6077
\end{verbatim}

\break

\normalsize
A model with f being a polynomial function can be fitted through:
\footnotesize
\begin{verbatim}
> modelLl <- ppm(ppLl, ~polynom(x, y, 3))
> modelLl
Nonstationary Poisson process

Trend formula: ~polynom(x, y, 3)
> AIC(modelLl)
6027
\end{verbatim}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{longleafTrend}
\end{figure}
\end{frame}

\begin{frame}[fragile]{The fitted Poisson process}
Now that the process is fitted, sampling can take place:
\footnotesize
\begin{verbatim}
rpoispp(lambdaFun, lmax=0.03, win=owin(c(0,200),c(0,200)))
\end{verbatim}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{longleafSim}
\end{figure}
\end{frame}

\begin{frame}{Additional options}
\begin{itemize}
\item intensity based on covariates (e.g. elevation)
\item intensity per category $\rightarrow$ multiple point process \break (e.g. sapling vs. adult)
\item interpoint interactions 
\item modelling of marks: (L, M\textbar L), (M, L\textbar M), (L,M)
\item testing for independence
\item GOF testing
\end{itemize}
\end{frame}

\subsection{Lattice}
\begin{frame}{Sudden infant death syndrome - North Carolina}
The following examples follow these of the spdep package and its vignettes \cite{Bivand2014}.
\begin{figure}
\centering
\includegraphics[width=\textwidth]{BIRk_nc}
\end{figure}
\end{frame}

\begin{frame}{Expected counts - probability map}
Assuming Poisson distributions with mean values set to the expected number of cases $ec$ per county based on the number of births $ec(county) = N_{Birth} \cdot rate$, the cumulated density of the observed values is derived. 
\begin{figure}
\includegraphics[width=\textwidth]{nc_probabilityMap}
\end{figure}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{CAR models}
The motivation of a \emph{conditional autoregressive model} is given by the conditional distribution
\begin{eqnarray*}
& & f\big( z(s_i) | \{ z(s_j): j\neq i\}\big) \\
&= & \frac{1}{\tau_i \sqrt{2\pi}} \exp\left( \frac{-\big(z(s_i) - \theta_i(\{ z(s_j): j\neq i\}) \big)^2}{2 \tau_i^2} \right)
\end{eqnarray*}
with 
$$ \theta_i(\{ z(s_j): j\neq i\}) = \mu_i + \sum\limits_{j=1}^{n} c_{ij}\big( z(s_j)-\mu_j \big) $$
while $(c_{ij})_{ij}$ with $c_{ij}=0$ unless the locations $s_i$ and $s_j$ are pairwise dependent, $c_{ij}\tau^2_i = c_{ji}\tau^2_j$ and $c_{ii}=0$. $\tau^2_i$.

\break 

The process $Z$ can then be modelled as 
$$ Z \sim \rm{Gau}\big(\boldsymbol{\mu},(I-C)^{-1}M\big)$$
$$ Z(s_i) = \mu_i + \sum\limits_{j=1}^{n} c_{ij}\big( Z(s_j)-\mu_j \big) + \nu_i$$
with $\boldsymbol{\nu} \sim \rm{Gau}(\mathbf{0}, M\big(I-C^t)\big)$ while $M = \rm{diag}(\tau^2_1,\dots,\tau^2_n)$, $C=(c_{ij})_{ij}$.

Typically, only a small number of conditioning sites is used assuming the Markov property
$$  f\big( z(s_i) | \{ z(s_j): j\neq i\}\big) =  f\big( z(s_i) | \{ z(s_j): j \in N_i \}\big)$$ with $N_i$ being the index set of selected neighbouring locations.
\end{frame}

\begin{frame}[allowframebreaks,fragile]{CAR Example: SIDS in North Carolina}

Freeman-Tukey transformed data

\begin{figure}
\centering
\includegraphics[width=\textwidth]{nc_FTtransformed}
\end{figure}

$$ Z \sim \rm{Gau}\big(\boldsymbol{\mu},(I-C)^{-1}M\big)$$
We assume a constant mean $\boldsymbol\mu=m$. The neighbourhood sets of the Markov Random Field are based on the distance between centroids of the counties ($\leq$~30~miles).
\begin{figure}
\centering
\includegraphics[width=\textwidth]{nc_neighbourhoods}
\end{figure}

The spatial dependence matrix $C$ is modelled as
$$ c_{ij} := \left\{ 
\begin{array}{ll}
\phi \cdot \frac{\min_{j\in N_i, i=1,\dots,n}(d_{ij}) }{d_{ij}} \cdot \sqrt{\frac{n_j}{n_i}},& j \in N_i \\
0,& j\notin N_i
\end{array}\right. $$
and the conditional variance by
$$ \tau^2_i := \frac{\tau^2}{n_i} $$
such that $c_{ij}\tau^2_j = c_{ji}\tau^2i$ and $M=\rm{diag}(\tau^2_1,\dots,\tau^2_n)$.

The set of parameters is $(m,\phi,\tau)$.

\framebreak
The call 
\footnotesize
\begin{verbatim}
car <- spautolm(ft.SID74 ~ 1, data=mdata.4, 
                listw=sids.nhbr.listw.4,
                weights=BIR74, family="CAR")
summary(car)
\end{verbatim}
\normalsize
yields estimates:
$\hat{m} = 2.84$, $\hat{\phi}=1.73$ and $\hat{\tau}=36.16$

\framebreak

Predicted transformed data
\begin{figure}
\centering
\includegraphics[width=\textwidth]{nc_SAR}
\end{figure}

\end{frame}

%\begin{frame}{Spatial structure}
%In lattice data, the spatial structure is often reduced to neighbourhood relations (e.g. which centroids are within 30~miles, as by \cite{Cressie1989}). \vspace{4pt}
%
%There is no \emph{between} neighbours - different than for random fields.
%\end{frame}

\begin{frame}[fragile]{SAR models}
A \emph{simultaneous autoregressive model} is given by
$$ Z \sim \rm{Gau}\big(\boldsymbol{\mu},(I-B)^{-1}\Lambda(I-B^t)^{-1}\big)$$
$$ Z(s_i) = \mu_i + \sum\limits_{j=1}^{n} b_{ij}\big( Z(s_j)-\mu_j \big) + \epsilon_i$$
with $\boldsymbol{\epsilon} \sim \rm{Gau}(\mathbf{0}, \Lambda\big)$, $B=(b_{ij})_{ij}$ while $b_{ii}=0$ and $b_{ij}$ captures the dependence of location $s_i$ on $s_j$. It is not necessarily $b_{ij}=b_{ji}$, but $(I-B)^{-1}$ is assumed to exist.
\end{frame}


\begin{frame}[allowframebreaks,fragile]{SAR Example: SIDS in North Carolina}

$$ Z \sim \rm{Gau}\big(\boldsymbol{\mu},(I-B)^{-1}\Lambda(I-B^t)^{-1}\big)$$
We assume again a constant mean $\boldsymbol\mu=m$. 

The spatial dependence matrix $B$ is modelled as
$$ b_{ij} := \phi \frac{1}{d_{ij} \cdot \sum_{j \in N_i} \frac{1}{d_{ij}}} $$
and the variance by
$$ \sigma^2_i := \frac{\sigma^2}{n_i} $$
such that $\Lambda=\rm{diag}(\sigma^2_1,\dots,\sigma^2_n)$.

The set of parameters is $(m,\phi,\sigma)$.

\framebreak
The call 
\footnotesize
\begin{verbatim}
sar <- spautolm(ft.SID74 ~ 1, data=mdata.4, 
                listw=sids.nhbr.listw.4,
                weights=BIR74, family="SAR")
summary(sar)
\end{verbatim}
\normalsize
yields estimates:
$\hat{m} = 2.94$, $\hat{\phi}=0.8683$ and $\hat{\sigma}=35.55$

\framebreak

Predicted transformed data
\begin{figure}
\centering
\includegraphics[width=\textwidth]{nc_CAR}
\end{figure}

\end{frame}

\subsection{Random Fields}
\begin{frame}{Random Fields}
The following examples follow these of the gstat package \cite{Pebesma2004} and the book \cite{Bivand2013}.\vspace{4pt}

Heavy metal concentrations in the soil along the Meuse riverbank have been sampled.\vspace{4pt}

How do the concentrations on the full grid look like?\vspace{4pt}

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{meuseObsIDW}
\end{figure}
\end{frame}

\begin{frame}{Stationarity and Isotropy}
\begin{description}
\item<1->[stationarity] The process "looks" the same at each location (e.g. mean and variance do not change from east to west)
\item<1->[isotropy] The dependence between locations is determined only by their separating distance neglecting the direction (e.g. locations 2~km apart along the north-south axis are as correlated as stations 2~km apart along the east-west axis)
\end{description}

Some tricks exist to weaken these assumptions (e.g. rotating and rescaling coordinates).
\end{frame}

\begin{frame}[allowframebreaks,fragile]{Variograms}
The dependence across space of a random field $Z$ is assessed using a \emph{variogram} $\gamma$:
$$ \gamma(h) = \frac{1}{2}\rm{E}\big(Z(s) - Z(s+h)\big)^2$$

the empirical estimator looks like 
$$ \hat{\gamma}(h) = \frac{1}{2 |N_h|} \sum\limits_{(i,j)\in N_h}\big(Z(s_i)-Z(s_j)\big)^2$$
while $N_h = \{(i,j) : h-\epsilon \leq ||s_i-s_j|| \leq h+\epsilon \}$

\framebreak
The \emph{sample variogram} is obtained through
\footnotesize
\begin{verbatim}
vgmMeuse <- variogram(zinc~1, meuse)
\end{verbatim}
\normalsize

\begin{figure}
\includegraphics[width=\textwidth]{meuseVgm}
\end{figure}

\framebreak
And a theoretical \emph{variogram model} can be fitted

\footnotesize
\begin{verbatim}
> head(vgm())
  short                        long
1   Nug                Nug (nugget)
2   Exp           Exp (exponential)
3   Sph             Sph (spherical)
4   Gau              Gau (gaussian)
5   Exc Exclass (Exponential class)
6   Mat                Mat (Matern)

> vgmModelMeuse <- fit.variogram(vgmMeuse, 
                                 vgm(0.6, "Sph", 1000, 0.1))
vgmModelMeuse
  model     psill    range
1   Nug  24813.21   0.0000
2   Sph 134753.99 831.2953
\end{verbatim}
\normalsize

\begin{figure}
\includegraphics[width=\textwidth]{meuseVgmModel}
\end{figure}
\end{frame}

\begin{frame}[allowframebreaks,fragile]{Kriging}
Certain variogram models can be used to parametrize a covariance matrix for a Gaussian random field over a finite set of locations $s_1$, \dots, $s_n$:
$$ Z \sim \rm{Gau}\big(\boldsymbol{\mu}, \Sigma\big) $$
while $\Sigma = (\sigma^2_{ij})_{ij}$ and $\sigma^2_{ij}=\sigma^2 - \gamma(||s_i-s_j||)$, $1 \leq i,j\leq n$ with $\sigma^2=\rm{Var}\big(Z(s)\big)$, $\boldsymbol{\mu}=(\mu_1,\dots,\mu_n)$.\vspace{4pt}

Predictions can be made using matrix inversion and matrix multiplications.

\framebreak
\footnotesize
\begin{verbatim}
krige(zinc~1, meuse, meuse.grid, model=vgmModelMeuse)
\end{verbatim}
\normalsize

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{meuseObsKrige}
\end{figure}

\framebreak

The model quantifies how \emph{uncertain} it is about the estimates through the kriging variance:

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{meuseKrigeVar}
\end{figure}
\end{frame}

\begin{frame}{Overview of kriging types}
\begin{description}
\item[simple kriging] the mean value is known
\item[ordinary kriging] prediction based on coordinates 
\item[universal kriging] prediction based on coordinates and additional regressors (distance to the river)
\item[co-kriging] the cross-variogram between two variables is as well exploit (zinc and lead)
\end{description}
\end{frame}

\section{Spatio-Temporal Extensions}

\begin{frame}{Spatio-Temporal Data}
$S \times T$ works as a data structure, but modelling needs to consider special properties of the product of space and time.
\begin{description}
\item<1->[direction] Today's values influence tomorrow, but will not take effect on yesterday's values.
\item<1->[anisotropy] What is the equivalent in terms of dependence of 1~m separation in seconds or minutes?
\end{description}

The easiest way to think of spatio-temporal data is as time slices - but this neglects the temporal dependence.\vspace{4pt}

After modelling temporal trend or periodicities, the residuals might be modelled as a spatio-temporal random field.
\end{frame}

\begin{frame}
\frametitle{The spatio-temporal variogram}
Extending the variogram to a twoplace function for spatio-temporal random fields $Z(s,t)$:
$$\gamma(h,u) = \rm{E}\big(Z(s,t)-Z(s+h,t+u) \big)^2$$
at any location $(s,t)$. And empirical version
$$ \hat{\gamma}(h,u) = \frac{1}{2 |N_{h,u}|} \sum\limits_{(i,j)\in N_{h,u}}\big(Z(s_i,t_i)-Z(s_j,t_j)\big)^2$$
while $N_{h,u} = \left\{(i,j) \left|\begin{array}{l}
h-\epsilon_s \leq ||s_i-s_j|| \leq h+\epsilon_s \\
u-\epsilon_t \leq t_i-t_j \leq u+\epsilon_t 
\end{array} \right.\right \}$
\end{frame}

\subsection{Spatio-Temporal Kriging}

\begin{frame}[allowframebreaks,fragile]{Spatio-Temporal Variogram Models}
Modelling dependence over space differently from the dependence over time yields another degree of freedom. 

\begin{description}
\item[metric] the temporal component is adjusted using only an anisotropy correction 
$$ \gamma_m(h,u) = \gamma_j(\sqrt{h^2+(\kappa\cdot u)^2}) $$
\item[separable] spatial and temporal component are added up neglecting interactions between space and time
\begin{eqnarray*}
\gamma_{sep}(h,u) &=& {\rm nug}\cdot {\bf1}_{h>0,u>0} \\
&+& {\rm sill} \cdot \big( \gamma_s(h)+\gamma_t(u)-\gamma_s(h)\gamma_t(u) \big)
\end{eqnarray*}
\item[product-sum] a product interaction term is added to the separable model
\begin{eqnarray*}
\gamma_{ps}(h,u) &=& {\rm nug}\cdot {\bf1}_{h>0,u>0} \\
&+& \gamma_s(h)+\gamma_t(u) - k \gamma_s(h)\gamma_t(u)
\end{eqnarray*}
The parameter $k$ needs to fulfil $0 < k \leq 1/(\max({\rm sill}_s,{\rm sill}_t))$.
\item[sum-metric] the covariance structure is composed out of spatial and temporal components added with a metric model capturing interactions
$$\gamma_{sm}(h,u)=  \gamma_s(h) + \gamma_t(u) + \gamma_j(\sqrt{h^2+(\kappa\cdot u)^2})$$
\end{description}
\end{frame}

\begin{frame}{Example}
A set of spatially spread time series of daily measurements: what does the random field look like all over Germany? \vspace{4pt}

\begin{figure}
\includegraphics[width=\textwidth]{stplot_ger_june}
\end{figure}
\end{frame}

\begin{frame}{Spatio-Temporal Sample Variogram}
\begin{figure}
\includegraphics[width=\textwidth]{empStVgm_wf}
\end{figure}
\end{frame}

\begin{frame}{variogram of the sum-metric model}
\begin{figure}
\includegraphics[width=\textwidth]{sumMetricStVgm}
\end{figure}
\end{frame}

\begin{frame}{variogram of all spatio-temporal models}
\begin{figure}
\includegraphics[width=\textwidth]{allStVgm}
\end{figure}
\end{frame}

\begin{frame}{kriged map for 10 days}
\begin{figure}
\includegraphics[width=\textwidth]{predSt}
\end{figure}
\end{frame}

\section{Copulas}
\begin{frame}{What if the world happens to be non-Gaussian?}
\begin{figure}
\includegraphics[width=0.8\textwidth]{ellipSymWireframe}
\end{figure}
\end{frame}

\begin{frame}[allowframebreaks]{Bivariate Copulas}
Copulas allow to model dependencies much more detailed than a typical correlation value. \vspace{4pt}

Instead of a single value, a full distribution is fitted describing dependence.

\begin{figure}
\includegraphics[width=0.5\textwidth]{normSmpl}
\includegraphics[width=0.5\textwidth]{copGumbSmpl}
\end{figure}

\begin{figure}
\includegraphics[width=0.5\textwidth]{expSmpl}
\includegraphics[width=0.5\textwidth]{mixSmpl}
\end{figure}

\begin{figure}
\includegraphics[width=0.5\textwidth]{normStudentSmpl}
\includegraphics[width=0.5\textwidth]{copStudentSmpl}
\end{figure}

See the \href{http://ifgi.uni-muenster.de/~b_grae02/indexCopulatheque.html}{copulatheque} for further interactive examples.
\end{frame}

\begin{frame}{Sklar's Theorem}
The power of copulas originates from Sklar's Theorem \cite{Sklar1959}: \vspace{4pt}

Any multivariate distribution $H$ can be decomposed into its marginal distribution functions $F_1$, \dots, $F_d$ and its copula $C$: 
$$ H(x_1, \dots, x_d) = C\big(F_1(x_1), \dots, F_d(x_d)\big) $$
\end{frame}

\subsection{Spatial Copulas}
\begin{frame}{Strength and Shape changes with distance}
The \emph{Bivariate Spatial Copula} is a convex combination of bivariate copula families parametrised by distance.
\begin{figure}
\centering
\animategraphics[loop,scale=0.45,poster=first,autoplay]{5}{animate/plot}{11}{80}
\end{figure}
\end{frame}

\begin{frame}{Spatial Vine Copulas}
A \emph{Vine Copula} connects bivariate copulas to multivariate copulas \cite{Aas2009,Cooke2011}.
\begin{figure}
\centering
\includegraphics[width=\textwidth]{can_Vine}
\end{figure}
\end{frame}

\begin{frame}{Spatial Vine Copula Prediction}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{quantilePredWireFrame}
\end{figure}
\end{frame}

\begin{frame}[allowframebreaks,fragile]{References}
\tiny
\bibliography{C:/Users/b_grae02/Documents/Forschung/commonBib}{}
\bibliographystyle{plain}
\end{frame}

\end{document}